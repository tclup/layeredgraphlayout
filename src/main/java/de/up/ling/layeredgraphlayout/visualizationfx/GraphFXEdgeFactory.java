package de.up.ling.layeredgraphlayout.visualizationfx;

import de.up.ling.layeredgraphlayout.layout.EdgeSegment;

import de.up.ling.layeredgraphlayout.layout.LayoutConfig;
import javafx.scene.Node;

import java.util.*;

/**
 * Created by jfschaefer on 7/31/15.
 */

public abstract class GraphFXEdgeFactory<E> {
    protected final LayoutConfig layoutConfig;

    public GraphFXEdgeFactory(LayoutConfig config) {
        layoutConfig = config;
    }

    public abstract Node getEdgeVisualization(E edge, ArrayList<EdgeSegment> segments);
}
